import { Injectable } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { ILivro } from "../interfaces/ILivro";
import { IReserva } from "../interfaces/IReserva";
import { IMeusLivros } from '../interfaces/IMeusLivros';

@Injectable()
export class LivroProvider {
    private livros: ILivro[] = [];
    private res: IReserva[] = [];
    private meus: IMeusLivros[] = [];
    constructor( public alertCtrl: AlertController ) {
        if (this.livros.length == 0)
            this.popularLivros();

    }

    popularLivros() {
      this.livros.push({ id: 1, titulo: "Anatomia do Desing", ano: 2017, img: "assets/capas/livro1.jpg", qtd: 3 });
      this.livros.push({ id: 2, titulo: "Como Se Faz", ano: 2016, img: "assets/capas/livro2.jpg", qtd: 2 });
      this.livros.push({ id: 3, titulo: "A Linguagem Invisivel da Tipografia", ano: 2015, img: "assets/capas/livro3.jpg", qtd: 1 });
      this.livros.push({ id: 4, titulo: "Desenho Para Designers", ano: 2016, img: "assets/capas/livro4.jpg", qtd: 5 });
      this.livros.push({ id: 5, titulo: "Letras Que Bailam", ano: 2016, img: "assets/capas/livro5.jpg", qtd: 1 });
      this.livros.push({ id: 6, titulo: "Sistemas De Informação", ano: 2013, img: "assets/capas/livro6.jpg", qtd: 2 });
      this.livros.push({ id: 7, titulo: "Construção Da Marca", ano: 2010, img: "assets/capas/livro7.jpg", qtd: 3 });
      this.livros.push({ id: 8, titulo: "ADG - 23", ano: 2017, img: "assets/capas/livro8.jpg", qtd: 2 });
      this.livros.push({ id: 9, titulo: "ADG - 21", ano: 2012, img: "assets/capas/livro9.jpg", qtd: 3 });
      this.livros.push({ id: 10, titulo: "I Nemici Di Francesco", ano: 2015, img: "assets/capas/livro10.jpg", qtd: 3 });
    }
    getLivros(): ILivro[] {
        return this.livros;
    }
    adicionarLivro(livro: ILivro) {
        if (livro.id == 0)
            livro.id = this.getNextID();
        this.livros.push(livro);
    }
    removerLivro(livro: ILivro) {
      let position = this.livros.findIndex((l: ILivro) => {
          return l.id == livro.id;
      })
      if ( livro.qtd >1 )
        this.livros[position].qtd = livro.qtd -1;
      else
        this.livros.splice(position, 1);
    }

    getReserva(): IReserva[]{
      return this.res;
  }

    reservarLivro(livro: IReserva){
      let positionLivro = this.livros.findIndex((l: ILivro) => {
        return l.id == livro.id;
      })
      this.res.push(livro);
    }

    confirmarLivro(livro: IMeusLivros){
      let positionReserva = this.res.findIndex((l: IReserva) => {
        return l.id == livro.id;
      })
      let positionLivro = this.livros.findIndex((l: ILivro) => {
        return l.id == livro.id;
      })
      this.livros[positionLivro].qtd = this.livros[positionLivro].qtd -1;
      this.meus.push(livro);

    }

    devolverLivro(livro: ILivro) {
      let position = this.res.findIndex((l: ILivro) => {
          return l.id == livro.id;
      })
      this.livros[position].qtd = livro.qtd +1;
      this.res.splice(position, 1);
    }

    alterarLivro(livro: ILivro) {
        let position = this.livros.findIndex((l: ILivro) => {
            return l.id == livro.id;
        })
        this.livros[position].titulo = livro.titulo;
        this.livros[position].ano = livro.ano;
        this.livros[position].img = livro.img;
    }

    getInstancia(): ILivro {
        return {
            id: 0,
            titulo: "",
            ano: null,
            img: "",
            qtd: null,
        };
    }

    private getNextID(): number {
        let nextId = 0;
        if (this.livros.length > 0) {
            nextId = Math.max.apply(
                Math, this.livros.map(function (o) { return o.id; })
            );
        }
        return ++nextId;
    }
}
