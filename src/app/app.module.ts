import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Camera } from "@ionic-native/camera";
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
//import { ListPage } from '../pages/list/list';
import { LivroListPage } from '../pages/livro-list/livro-list';
import { LivroProvider } from "../providers/livro";
import { LivroAddPage } from "../pages/livro-add/livro-add";
import { LivroDetailsPage } from "../pages/livro-details/livro-details";
import { AutorListPage } from '../pages/autor-list/autor-list';
import { AutorProvider } from "../providers/autor";
import { ReservaProvider } from "../providers/reserva";
import { AutorAddPage } from "../pages/autor-add/autor-add";
import { AutorDetailsPage } from "../pages/autor-details/autor-details";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LivroAutorProvider } from '../providers/livro-autor';
import { LoginProvider } from '../providers/login';
import { SigninPage } from '../pages/signin/signin';
import { SignupPage } from '../pages/signup/signup';
import { ChatPage } from '../pages/chat/chat';
import { SocialPage } from '../pages/social/social';
import { SocialCardComponent } from '../components/social-card/social-card';
import { CarrinhoPage } from '../pages/carrinho/carrinho';
import { MeusLivrosProvider } from '../providers/meus-livros';
import { MeusLivrosPage } from '../pages/meus-livros/meus-livros';

export const FIREBASECONFIG = {
  apiKey: "AIzaSyCLlYK7NNNPYCVN85qY3YlOqZXxSoAbwgc",
  authDomain: "bibliotecaunip-666mec.firebaseapp.com",
  databaseURL: "https://bibliotecaunip-666mec.firebaseio.com",
  projectId: "bibliotecaunip-666mec",
  storageBucket: "bibliotecaunip-666mec.appspot.com",
  messagingSenderId: "433812505157"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    //  ListPage,
    LivroListPage,
    LivroAddPage,
    LivroDetailsPage,
    AutorListPage,
    AutorAddPage,
    AutorDetailsPage,
    SigninPage,
    SignupPage,
    ChatPage,
    SocialPage,
    SocialCardComponent,
    CarrinhoPage,
    MeusLivrosPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASECONFIG),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    //  ListPage,
    LivroListPage,
    LivroAddPage,
    LivroDetailsPage,
    AutorListPage,
    AutorAddPage,
    AutorDetailsPage,
    SigninPage,
    SignupPage,
    ChatPage,
    SocialPage,
    SocialCardComponent,
    CarrinhoPage,
    MeusLivrosPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    LivroProvider,
    LivroAddPage,
    LivroDetailsPage,
    AutorProvider,
    AutorAddPage,
    AutorDetailsPage,
    Camera,
    AutorProvider,
    LivroAutorProvider,
    LoginProvider,
    CarrinhoPage,
    ReservaProvider,
    MeusLivrosProvider
  ]
})
export class AppModule { }
