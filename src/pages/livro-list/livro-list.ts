import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LivroProvider } from "../../providers/livro";
import { ILivro } from "../../interfaces/ILivro";
import { LivroAddPage } from "../livro-add/livro-add";
import { LivroDetailsPage } from "../livro-details/livro-details";
@Component({
    selector: 'page-livro-list',
    templateUrl: 'livro-list.html'
})
export class LivroListPage {
    selectedItem: any;
    icons: string[];
    items: Array<ILivro>;
    pesquisa: string;
    visibilidade: boolean;
    itemsFilter: Array<ILivro>;

    novoItem(event, item) {
        this.navCtrl.push(LivroAddPage, { });
        }

    abrirPesquisa(event) {
        this.visibilidade = true;
    }


    pesquisar(event) {
        this.itemsFilter = this.items.filter((i) => {
            if (i.titulo.indexOf(this.pesquisa) >= 0) {
                return true;
            }
            return false;
        })
    }

    cancelarPesquisa() {
        this.visibilidade = false;
        this.pesquisa = "";
        this.pesquisar(null);
    }

    constructor(public navCtrl: NavController, public navParams: NavParams, public livroProvider: LivroProvider) {

        this.items = livroProvider.getLivros();
        this.itemsFilter = this.items;
        this.visibilidade = false;
    }

    itemTapped(event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(LivroDetailsPage, {
            item: item
        });
    }

    ionViewWillEnter() {
        this.items = this.livroProvider.getLivros();
        this.itemsFilter = this.items;
    }
}