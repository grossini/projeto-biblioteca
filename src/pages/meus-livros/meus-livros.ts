import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CarrinhoPage } from '../carrinho/carrinho';
import { LivroListPage } from '../livro-list/livro-list'
import { LivroProvider } from "../../providers/livro";
import { ILivro } from "../../interfaces/ILivro";
import { IReserva } from '../../interfaces/IReserva';
import { LivroDetailsPage } from "../livro-details/livro-details";

@IonicPage()
@Component({
  selector: 'page-meus-livros',
  templateUrl: 'meus-livros.html',
})
export class MeusLivrosPage {
    livro: ILivro;
    items: Array<IReserva>;
    visibilidade: boolean;
    itemsFilter: Array<IReserva>;
    pesquisa: string;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  novoItem(event, item) {
    this.navCtrl.push(LivroListPage, { });
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MeusLivrosPage');
  }

}
