export interface IAutor {
    id: number;
    nome: string;
    idade: number;
    img: string;
    }