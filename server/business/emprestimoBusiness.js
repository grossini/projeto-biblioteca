"use strict";

class EmprestimoBusiness {

    findAll(usuario, callbackEmprestimo) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query(' SELECT * FROM Emprestimo' +
            'JOIN LivroEmprestimo ON Emprestimo.emId = LivroEmprestimo.emId' +
            'JOIN Livro ON LivroEmprestimo.lvId = Livro.lvId' +
            'WHERE Emprestimo.usId =' + usuario.usId + ' AND Emprestimo.emDataDevolucao = 0000-00-00;',
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackEmprestimo(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    post(emprestimo, callbackEmprestimo) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("INSERT INTO Emprestimo (usId, emDataInicio, emDataFim, emDataDevolucao) values (" +
            emprestimo.usId + ", CURDATE(), DATE_ADD(CURDATE(), INTERVAL 10 DAY),'" +
            emprestimo.emDataDevolucao + "')",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackEmprestimo(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    update(emprestimo, callbackEmprestimo) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("UPDATE Emprestimo SET emDataInicio = '" +
            emprestimo.emDataInicio + "', emDataFim ='" +
            emprestimo.emDataFim + "', emDataDevolucao ='" +
            emprestimo.emDataDevolucao + "' WHERE emId = " +
            emprestimo.emId + "",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackEmprestimo(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    delete(emprestimo, callbackEmprestimo) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("DELETE FROM Emprestimo WHERE emId = " +
            emprestimo.emId + " AND emDataDevolucao <> 0000-00-00",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackEmprestimo(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }
}
module.exports = EmprestimoBusiness;
